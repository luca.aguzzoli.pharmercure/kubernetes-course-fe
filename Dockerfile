FROM node:16-alpine as builder

WORKDIR /app
COPY package.json .

RUN npm install

COPY . .

RUN npm run build

FROM nginx

COPY nginx.conf /etc/nginx/nginx.conf
COPY --from=builder /app/build /usr/share/nginx/www

RUN mkdir -p /var/log/app_engine
RUN chmod -R a+r /usr/share/nginx/www